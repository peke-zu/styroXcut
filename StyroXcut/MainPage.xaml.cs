﻿using System.Collections.ObjectModel;
using System.Globalization;
using System.Threading.Tasks;
using Windows.ApplicationModel.Resources;
using Windows.Foundation;
using Windows.Storage;
using Windows.UI.Popups;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Xaml.Shapes;
using System;
using System.IO;
using Windows.ApplicationModel;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace StyroXcut
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {

        public MainPage()
        {
            this.InitializeComponent();

            Loaded += MainPage_Loaded;
        }

        private void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            twist_1.LostFocus += twist_1_LostFocus;
            twist_2.LostFocus += twist_2_LostFocus;
            sweep.LostFocus += sweep_LostFocus;
            length_1.LostFocus += length_1_LostFocus;
            length_2.LostFocus += length_2_LostFocus;
            panel_length.LostFocus += panel_length_LostFocus;

            Load_settings();
            Load_values();
        }

        private void Load_settings()
        {
            CultureInfo customCulture = new CultureInfo("fi-FI");
            customCulture.NumberFormat.NumberDecimalSeparator = ".";

            ApplicationDataContainer localSettings = ApplicationData.Current.LocalSettings;

            Globals.x_spr = double.Parse(get_setting("x_spr", "200"), customCulture);
            Globals.y_spr = double.Parse(get_setting("y_spr", "200"), customCulture);
            Globals.x_pitch = double.Parse(get_setting("x_pitch", "3.0"), customCulture);
            Globals.y_pitch = double.Parse(get_setting("y_pitch", "3.0"), customCulture);
            Globals.speed = double.Parse(get_setting("speed", "200"), customCulture);

        }

        private async void Load_values()
        {
            length_1.Text = get_setting("length_1", "200");
            length_2.Text = get_setting("length_2", "300");
            twist_1.Text = get_setting("twist_1", "0");
            twist_2.Text = get_setting("twist_2", "0");
            sweep.Text = get_setting("sweep", "0");
            panel_length.Text = get_setting("panel_length", "1000");
            left.IsChecked = get_bool_setting("left", true);
            right.IsChecked = get_bool_setting("right", false);
            left_file.Text = get_setting("left_file", "sample sd7032");
            right_file.Text = get_setting("right_file", "sample sd7032");

            length_1_LostFocus(length_1, null);
            length_2_LostFocus(length_2, null);
            twist_1_LostFocus(twist_1, null);
            twist_2_LostFocus(twist_2, null);
            panel_length_LostFocus(panel_length, null);
            sweep_LostFocus(sweep, null);

            if (left_file.Text == "sample sd7032")
            {
                Globals.DATA1 = await ReadAssetsFile();
            }
            else
            {
                Globals.DATA1 = await ReadFile("1.dat");
            }
            if (right_file.Text == "sample sd7032")
            {
                Globals.DATA2 = await ReadAssetsFile();
            }
            else
            {
                Globals.DATA2 = await ReadFile("2.dat");
            }
            Globals.coordinates_1 = Read_profile(Globals.DATA1);
            Globals.coordinates_2 = Read_profile(Globals.DATA2);
            Draw_profile(Globals.coordinates_1, Globals.DATA1, canvas1, Globals.twist_1);
            Draw_profile(Globals.coordinates_2, Globals.DATA2, canvas2, Globals.twist_2);
        }

        private async Task<string> ReadAssetsFile()
        {
            string filename = @"Assets\sample_profiles\sd7032.dat";

            StorageFile sFile = await Package.Current.InstalledLocation.GetFileAsync(filename);
            Stream fileStream = await sFile.OpenStreamForReadAsync();
            StreamReader reader = new StreamReader(fileStream);
            string text = reader.ReadToEnd();
            return text;
        }

        private void Save_values()
        {
            ApplicationDataContainer screen_values = ApplicationData.Current.LocalSettings;
            screen_values.Values["length_1"] = length_1.Text;
            screen_values.Values["length_2"] = length_2.Text;
            screen_values.Values["twist_1"] = twist_1.Text;
            screen_values.Values["twist_2"] = twist_2.Text;
            screen_values.Values["sweep"] = sweep.Text;
            screen_values.Values["panel_length"] = panel_length.Text;
            screen_values.Values["left"] = left.IsChecked;
            screen_values.Values["right"] = right.IsChecked;
            screen_values.Values["left_file"] = left_file.Text;
            screen_values.Values["right_file"] = right_file.Text;
        }

        private string get_setting(string setting, string default_value)
        {
            ApplicationDataContainer localSettings = ApplicationData.Current.LocalSettings;

            try
            {
                string tst = localSettings.Values[setting].ToString();
            }
            catch (Exception e)
            {
                localSettings.Values[setting] = default_value;
                return default_value;
            }

            if (!localSettings.Values[setting].ToString().Equals(null) & !localSettings.Values[setting].ToString().Equals(""))
            {
                return localSettings.Values[setting].ToString();
            }
            else
            {
                localSettings.Values[setting] = default_value;
                return default_value;
            }
        }

        private bool get_bool_setting(string setting, bool default_value)
        {
            ApplicationDataContainer localSettings = ApplicationData.Current.LocalSettings;

            try
            {
                bool tst = (bool)localSettings.Values[setting];
                return tst;
            }
            catch (Exception e)
            {
                localSettings.Values[setting] = default_value;
                return default_value;
            }
            /*
            if (!localSettings.Values[setting].ToString().Equals(null) & !localSettings.Values[setting].ToString().Equals(""))
            {
                return (bool)localSettings.Values[setting];
            }
            else
            {
                localSettings.Values[setting] = default_value;
                return default_value;
            }*/
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);
        }

        private void panel_length_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox txt = sender as TextBox;
            double num = number_value(txt.Text);
            if (num == -9999)
            {
                Show_message("Kentän_muoto");
            }
            else
            {
                Globals.panel_length = num;
                Draw_image_2();
            }

        }

        private void length_1_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox txt = sender as TextBox;
            double num = number_value(txt.Text);
            if (num == -9999)
            {
                Show_message("Kentän_muoto");
            }
            else
            {
                Globals.length_1 = num;
                Draw_image_2();
            }
        }

        private void length_2_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox txt = sender as TextBox;
            double num = number_value(txt.Text);
            if (num == -9999)
            {
                Show_message("Kentän_muoto");
            }
            else
            {
                Globals.length_2 = num;
                Draw_image_2();
            }
        }

        private double number_value(string text_string)
        {
            CultureInfo customCulture = new CultureInfo("fi-FI");
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            try
            {
                double test_value = double.Parse(text_string, customCulture.NumberFormat);
                return test_value;
            }
            catch
            {
                return -9999;
            }
        }

        private async void Show_message(string message)
        {
            var resourceLoader = ResourceLoader.GetForCurrentView();
            var dialog = new MessageDialog(resourceLoader.GetString(message));
            await dialog.ShowAsync();
        }

        private void sweep_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox txt = sender as TextBox;
            double num = number_value(txt.Text);
            if (num == -9999)
            {
                Show_message("Kentän_muoto");
            }
            else
            {
                Globals.sweep = num;
                Draw_image(canvas1, Globals.coordinates_1, Globals.twist_1);
                Draw_image(canvas2, Globals.coordinates_2, Globals.twist_2);
                Draw_image_2();
            }
        }

        private void twist_1_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox txt = sender as TextBox;
            double num = number_value(txt.Text);
            if (num == -9999)
            {
                Show_message("Kentän_muoto");
            }
            else
            {
                Globals.twist_1 = num;
                Draw_image(canvas1, Globals.coordinates_1, Globals.twist_1);
            }
        }

        private void twist_2_LostFocus(object sender, RoutedEventArgs e)
        {
            TextBox txt = sender as TextBox;
            double num = number_value(txt.Text);
            if (num == -9999)
            {
                Show_message("Kentän_muoto");
            }
            else
            {
                Globals.twist_2 = num;
                Draw_image(canvas2, Globals.coordinates_2, Globals.twist_2);
            }
        }

        public async Task<string> Pick_file(TextBlock textBlock)
        {
            StorageFile file;
            var picker = new Windows.Storage.Pickers.FileOpenPicker();
            picker.ViewMode = Windows.Storage.Pickers.PickerViewMode.Thumbnail;
            picker.SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.PicturesLibrary;
            picker.FileTypeFilter.Add(".dat");
            picker.FileTypeFilter.Add(".txt");

            file = await picker.PickSingleFileAsync();
            if (file != null)
            {
                // Application now has read/write access to the picked file
                textBlock.Text = file.Path;
            }
            else
            {
                textBlock.Text = "Cancelled";
            }
            string sisältö = await Read_file(file);
            return sisältö;
        }

        private async void button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string content_str = await Pick_file(left_file);
                Globals.DATA1 = content_str;
                Globals.coordinates_1 = Read_profile(content_str);
                Draw_profile(Globals.coordinates_1, content_str, canvas1, Globals.twist_1);
            }
            catch
            {
            }
        }

        private async void button1_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string content_string = await Pick_file(right_file);
                Globals.DATA2 = content_string;
                Globals.coordinates_2 = Read_profile(content_string);
                Draw_profile(Globals.coordinates_2, content_string, canvas2, Globals.twist_2);
            }
            catch
            {

            }
        }

        async void Draw_profile(ObservableCollection<Coordinate> coords, string profile_data, Canvas canvas, double kierto)
        {
            if (coords.Count == 0)
            {
                var resourceLoader = ResourceLoader.GetForCurrentView();
                var dialog = new MessageDialog(resourceLoader.GetString("Tiedostovirhe"));
                await dialog.ShowAsync();
            }
            else Draw_image(canvas, coords, kierto);
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Cut), null);
        }

        private void Draw_image(Canvas canv, ObservableCollection<Coordinate> coordinates, double twist)
        {
            var uiSettings = new UISettings();

            double canvas_length = canv.ActualWidth;
            double canvas_height = canv.ActualHeight;


            // Add the Polyline Element
            Polyline myPolyline = new Polyline();
            Windows.UI.Color c = uiSettings.GetColorValue(UIColorType.Foreground);
            //c = Windows.UI.Colors.SteelBlue;
            Brush brush = new SolidColorBrush(c);
            myPolyline.Stroke = brush;
            myPolyline.StrokeThickness = 2;
            myPolyline.FillRule = FillRule.EvenOdd;

            //Haetaan profiilin keskilinjan korkeus
            double maxY = -1000;
            double minY = 1000;
            foreach (Coordinate coord in coordinates)
            {
                Coordinate coord_2 = Twist(coord, twist);
                if (coord_2.Y < minY) minY = coord_2.Y;
                if (coord_2.Y > maxY) maxY = coord_2.Y;
            }
            double center_y = (maxY + minY) / 2;
            double height_y = maxY - minY;
            double rel_y = center_y / height_y;
            if (height_y * canvas_length > canvas_height)
            {
                canvas_length = canvas_length * (canvas_height / (height_y * canvas_length));
            }

            PointCollection myPointCollection = new PointCollection();
            foreach (Coordinate coord in coordinates)
            {
                Coordinate coord_2 = Twist(coord, twist);
                coord_2 = Sweep(coord_2, Globals.sweep);
                //                Point point = new Point(coord.X * canvas_length, canvas_height/2 + (-1 * coord.Y) * canvas_length);
                Point point = new Point(coord_2.X * canvas_length, canvas_height - (((coord_2.Y)) * canvas_length + canvas_height / 2 - center_y * canvas_length));
                myPointCollection.Add(point);
            }

            myPolyline.Points = myPointCollection;
            canv.Children.Clear();
            canv.Children.Add(myPolyline);
        }

        private async Task<string> Read_file(StorageFile file)
        {
            string text = await FileIO.ReadTextAsync(file);
            return text;
        }

        private ObservableCollection<Coordinate> Read_profile(string text )
        {
            //Eka rivi pois
            //            text=text.Substring(text.IndexOf('\n'));
            CultureInfo customCulture = new CultureInfo("fi-FI");
            customCulture.NumberFormat.NumberDecimalSeparator = ".";

            ObservableCollection<Coordinate> coordinates=new ObservableCollection<Coordinate>();

            char[] delimiterChars = { ' ', '\t', '\r', '\n' };
            string[] words = text.Split(delimiterChars);
            int i = 0, j = 0;
            string previousword = "";
            foreach (string word in words)
            {
                if (word.Length > 2)
                {
                    try
                    {
                        if (i == 0 & double.Parse(word, customCulture.NumberFormat) == 1 || double.Parse(word, customCulture.NumberFormat) == 0) i = 1;
                    }
                    catch
                    {

                    }
                    if (i > 0) //Aloitetaan kolmannesta stringistä i=2
                    {
                        if (j == 1)
                        {
                            Coordinate coordinate = new Coordinate(double.Parse(previousword, customCulture.NumberFormat), double.Parse(word, customCulture.NumberFormat));
                            coordinates.Add(coordinate);
                            j = 0;
                        }
                        else
                        {
                            j = 1;
                            previousword = word;
                        }
                    }
                }
            }
            //            ObservableCollection<Coordinate> normalisoitu = new ObservableCollection<Coordinate>();
            ObservableCollection<Coordinate> sorted = new ObservableCollection<Coordinate>();
            //            normalisoitu = normalize_profile(coordinates);
            if (coordinates.Count > 0)
            {
                sorted = do_sorted(coordinates);
                return sorted;
            }
            else return coordinates;
        }

        private void Page_SizeChanged(object sender, SizeChangedEventArgs e)
        {

        }

        private void number_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            int i = e.Key.ToString().IndexOf("Number");
            int j = e.Key.ToString().IndexOf("190");
            int k = e.Key.ToString().IndexOf("189");

            if (i == -1 & j == -1 & k == -1)
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }

        }
/*
        private ObservableCollection<Coordinate> muokkaa_profiili(ObservableCollection<Coordinate> coordinates, double kierto_rad, double nuoli_rad)
        {

            return coordinates;
        }
*/
        private ObservableCollection<Coordinate> do_sorted(ObservableCollection<Coordinate> coordinates)
        {
            ObservableCollection<Coordinate> helpvar = new ObservableCollection<Coordinate>();
            ObservableCollection<Coordinate> helpvar_2 = new ObservableCollection<Coordinate>();

            int i = 0;
            //Jos ei ala ykkösellä, niin sortataan
            if (coordinates[0].X - 1 != 0)
            {
                foreach (Coordinate coord in coordinates)
                {
                    helpvar.Add(coord);
                    if (coord.X - 1 == 0)
                    {
                        break;
                    }
                    i++;
                }
                for (int j = i; j > -1; j--)
                {
                    helpvar_2.Add(helpvar[j]);
                }
                for (int k = i + 1; k < coordinates.Count; k++)
                {
                    helpvar_2.Add(coordinates[k]);
                }
                return helpvar_2;
            }
            else return coordinates;
        }

        private ObservableCollection<Coordinate> normalize_profile(ObservableCollection<Coordinate> coordinates)
        {
            double d = -1000;
            ObservableCollection<Coordinate> help_var = new ObservableCollection<Coordinate>();

            foreach (Coordinate coord in coordinates)
            {
                if (coord.X > d) d = coord.X;
            }
            foreach (Coordinate coord in coordinates)
            {
                Coordinate k = new Coordinate(coord.X / d, coord.Y / d);
                help_var.Add(k);
            }
            //Jos on valmiiksi normalisoitu
            if (d == 1)
            {
                return coordinates;
            }
            else return help_var;
        }

        private Coordinate Twist(Coordinate xy, double twist_degrees)
        {
            double twist_rad = (Math.PI / 180) * twist_degrees;

            double hypo = Math.Sqrt(Math.Pow(xy.X, 2) + Math.Pow(xy.Y, 2));
            double angle = 0;
            if (xy.X == 0)
            {
                angle = Math.PI;
            }
            else angle = Math.Atan(xy.Y / xy.X);

            double new_angle = angle + twist_rad;

            double new_x = Math.Cos(new_angle) * hypo;
            double new_y = Math.Sin(new_angle) * hypo;
            Coordinate new_xy = new Coordinate(new_x, new_y);

            return new_xy;
        }

        private Double Sweep_shortening(double sweep_degrees, double panel_length, double short_profile, double long_profile)
        {
            double angle = Math.Atan(Math.Abs(long_profile - short_profile) / panel_length);
            //Vähennetään lyhyestä profiilista
            double sweep_rad = (Math.PI / 180) * sweep_degrees;
            if (sweep_rad > angle)
            {
                double opposite_1 = Math.Tan(sweep_rad) * panel_length - Math.Abs(long_profile - short_profile); //Kärjen suuntainen pitkä sivu
                double gamma = Math.Atan(opposite_1 / panel_length); //Suorakaiteen "yläpuolella" oleva nuolikulman osa
                double beta = sweep_rad - gamma; //Suorakaiteen "alapuolella" oleva nuolikulman osa
                double opposite_2 = Math.Sin(gamma) * short_profile; //Kärjen etupuolelle jäävä ylimääräinen kolmion sivu
                double opposite_3 = opposite_2 * Math.Tan(beta); //Kärjen sivulle jäävä ylimääräinen kolmion sivu, joka vähennetään pituudesta
                return opposite_3;
            }
            else return 0;
        }

        private Double Sweep_gamma(double sweep_degrees, double panel_length, double short_profile, double long_profile)
        {
            double angle = Math.Atan(Math.Abs(long_profile - short_profile) / panel_length);
            //Vähennetään lyhyestä profiilista
            double sweep_rad = (Math.PI / 180) * sweep_degrees;
            if (sweep_rad > angle)
            {
                double opposite_1 = Math.Tan(sweep_rad) * panel_length - Math.Abs(long_profile - short_profile); //Kärjen suuntainen pitkä sivu
                double gamma = Math.Atan(opposite_1 / panel_length); //Suorakaiteen "yläpuolella" oleva nuolikulman osa
                double beta = sweep_rad - gamma; //Suorakaiteen "alapuolella" oleva nuolikulman osa
                double opposite_2 = Math.Sin(gamma) * short_profile; //Kärjen etupuolelle jäävä ylimääräinen kolmion sivu
                double opposite_3 = opposite_2 * Math.Tan(beta); //Kärjen sivulle jäävä ylimääräinen kolmion sivu, joka vähennetään pituudesta
                return gamma;
            }
            else return 0;
        }

        private Coordinate Sweep(Coordinate xy, double sweep_angle)
        {
            double sweep_rad = (Math.PI / 180) * sweep_angle;
            double new_x = xy.X * Math.Cos(sweep_rad);
            Coordinate new_xy = new Coordinate(new_x, xy.Y);
            return new_xy;
        }

        private void number_2_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            int i = e.Key.ToString().IndexOf("Number");
            int j = e.Key.ToString().IndexOf("190");

            if (i == -1 & j == -1)
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }

        private void Draw_image_2()
        {
            Canvas canv = canvas3;
            var uiSettings = new UISettings();

            double canvas_length = canv.ActualWidth;
            double canvas_height = canv.ActualHeight;

            double short_profile = 0, long_profile = 0;
            if (Globals.length_1 > Globals.length_2)
            {
                long_profile = Globals.length_1;
                short_profile = Globals.length_2;
            }
            else
            {
                long_profile = Globals.length_2;
                short_profile = Globals.length_1;
            }

            double gamma = Sweep_gamma(Globals.sweep, Globals.panel_length, short_profile, long_profile);

            double len_1 = Math.Cos(gamma) * Globals.length_1;
            double len_2 = Math.Cos(gamma) * Globals.length_2;
            double part_length = Globals.panel_length / Math.Cos(gamma) + Math.Tan(gamma) * len_2;
            double x_move_1 = Math.Tan(gamma) * len_1;
            double x_move_2 = Math.Tan(gamma) * len_2;

            double x1 = 0, x2 = 0, x3 = 0, x4 = 0, y1 = 0, y2 = 0, y3 = 0, y4 = 0;
            if (Globals.left)
            {
                x1 = 0;
                x2 = x1 + x_move_1;
                x3 = part_length;
                x4 = part_length - x_move_2;
                y1 = 0;
                y2 = len_1;
                y3 = len_2;
                y4 = 0;
            }
            else
            {
                x1 = x1 + x_move_2;
                x2 = 0;
                x3 = part_length-x_move_1;
                x4 = part_length;
                y1 = 0;
                y2 = len_2;
                y3 = len_1;
                y4 = 0;
            }


            // Add the Polyline Element
            Polyline myPolyline = new Polyline();
            Windows.UI.Color c = uiSettings.GetColorValue(UIColorType.Foreground);
            //c = Windows.UI.Colors.SteelBlue;
            Brush brush = new SolidColorBrush(c);
            myPolyline.Stroke = brush;
            myPolyline.StrokeThickness = 2;
            myPolyline.FillRule = FillRule.EvenOdd;

            PointCollection myPointCollection = new PointCollection();
            double marginal = canvas_height * 1.0 / 20.0;
            double coeff = 19.0 / 20.0 * canvas_length / part_length;
            double y = new double();
            y = y2;
            if (y3 > y2) y = y3;
            if ((coeff * y + 2 * marginal) > canvas_height) coeff = (canvas_height - 2 * marginal) / y;

            Point point = new Point(marginal + coeff * x1, canvas_height - marginal - coeff * y1);
            myPointCollection.Add(point);
            point = new Point(marginal + coeff * x2, canvas_height - marginal - coeff * y2);
            myPointCollection.Add(point);
            point = new Point(marginal + coeff * x3, canvas_height - marginal - coeff * y3);
            myPointCollection.Add(point);
            point = new Point(marginal + coeff * x4, canvas_height - marginal - coeff * y4);
            myPointCollection.Add(point);
            point = new Point(marginal + coeff * x1, canvas_height - marginal - coeff * y1);
            myPointCollection.Add(point);

            myPolyline.Points = myPointCollection;
            canv.Children.Clear();
            canv.Children.Add(myPolyline);
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            RadioButton rb = sender as RadioButton;

            if (rb != null)
            {
                string vasoik = rb.Tag.ToString();
                switch (vasoik)
                {
                    case "left":
                        Globals.left = true;
                        Draw_image_2();
                        break;
                    case "right":
                        Globals.left = false;
                        Draw_image_2();
                        break;
                }
            }
        }

        private void Settings_AppBarButton_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(Settings), null);
            //AsetuksetPane.IsPaneOpen = true;
        }

        private void Ready_Click_1(object sender, RoutedEventArgs e)
        {
            Save_values();
            SaveFile("1.dat", Globals.DATA1);
            SaveFile("2.dat", Globals.DATA2);
            Globals.coordinates_1_C.Clear();
            //Molemmat päädyt kierretään
            Globals.coordinates_1 = do_sorted(Globals.coordinates_1); //Jos epästandardi tiedosto
            foreach (Coordinate coord in Globals.coordinates_1)
            {
                Globals.coordinates_1_C.Add(Twist(coord, Globals.twist_1));
            }
            Globals.coordinates_2_C.Clear();
            Globals.coordinates_2 = do_sorted(Globals.coordinates_2);
            foreach (Coordinate coord in Globals.coordinates_2)
            {
                Globals.coordinates_2_C.Add(Twist(coord, Globals.twist_2));
            }

            //Lasketaan lyhyemmälle päälle uusi leikkauspituus, jos nuolikulmaa enemmän kuin pituuserot = leikataan jättöreuna suorassa
            double long_profile = 0, short_profile = 0;
            if (Globals.length_1 > Globals.length_2)
            {
                long_profile = Globals.length_1;
                short_profile = Globals.length_2;
            }
            else
            {
                long_profile = Globals.length_2;
                short_profile = Globals.length_1;
            }

            double ss = Sweep_shortening(Globals.sweep, Globals.panel_length, short_profile, long_profile);

            if (Globals.length_1 > Globals.length_2)
            {
                Globals.length_2_C = Globals.length_2 - ss;
                Globals.length_1_C = Globals.length_1;
            }
            else
            {
                Globals.length_2_C = Globals.length_2;
                Globals.length_1_C = Globals.length_1 - ss;
            }

            Globals.step_cnt_1 = new ObservableCollection<Step>();
            Globals.step_cnt_1 = Count_steps(Globals.coordinates_1_C, Globals.length_1_C);
            Globals.step_cnt_2 = new ObservableCollection<Step>();
            Globals.step_cnt_2 = Count_steps(Globals.coordinates_2_C, Globals.length_2_C);

            do_cut.IsEnabled = true;

        }

        private ObservableCollection<Step> Count_steps(ObservableCollection<Coordinate> coordinates, double length)
        {
            ObservableCollection<Step> steps = new ObservableCollection<Step>();
            bool first = true;
            Coordinate old_coord = null;
            double x_step_length = Globals.x_pitch / Globals.x_spr;
            double y_step_length = Globals.y_pitch / Globals.y_spr;
            double x_cumul = 0;
            double y_cumul = 0;
            int x_dir = 0, old_x_dir = 0;
            int y_dir = 0, old_y_dir = 0;

            foreach (Coordinate coord in coordinates)
            {
                //Ekakierroksella
                if (first)
                {
                    first = false;
                }
                else
                {
                    double x_diff = (coord.X - old_coord.X) * length;
                    x_dir = 1;
                    if (x_diff < 0) x_dir = 0;

                    double y_diff = (coord.Y - old_coord.Y) * length;
                    y_dir = 1;
                    if (y_diff < 0) y_dir = 0;

                    //                    double kertymä = 0;
                    //X-pätkän length suurempi kuin Y-pätkän
                    if (Math.Abs(x_diff) > Math.Abs(y_diff))
                    {
                        int step_cnt = (int)(0.5 + Math.Abs(x_diff) / x_step_length); //Pätkällä tämän verran step_cnt
                        for (int i = 0; i < step_cnt; i++)
                        {
                            int z_step = 0;
                            if (y_dir == old_y_dir)
                            {
                                y_cumul = y_cumul + Math.Abs(y_diff) / step_cnt; //Kun pitemmällä suunnalla one askel, lyhyempi kerryttää vähemmän, kunnes one tulee täyteen
                            }
                            else //Suunta vaihtui, joten kertymän suunta vaihtui
                            {
                                y_cumul = y_cumul - Math.Abs(y_diff) / step_cnt; //Kun pitemmällä suunnalla one askel, lyhyempi kerryttää vähemmän, kunnes one tulee täyteen
                            }
                            if (Math.Abs(y_cumul) > y_step_length)
                            {
                                z_step = 1;
                                if (y_cumul > 0) y_cumul = y_cumul - y_step_length; //Yksi askel pois kertymästä
                                if (y_cumul < 0) y_cumul = y_cumul + y_step_length; //Yksi askel pois kertymästä
                            }
                            steps.Add(new Step(x_dir, 1, y_dir, z_step)); //X-akselille one askel, kertyvälle vasta kun one täyteen
                        }
                    }
                    //Y-pätkän length suurempi kuin X-pätkän
                    else
                    {
                        int step_cnt = (int)(0.5 + Math.Abs(y_diff) / y_step_length); //Pätkällä tämän verran step_cnt
                        for (int i = 0; i < step_cnt; i++)
                        {
                            int z_step = 0;
                            if (x_dir == old_x_dir)
                            {
                                x_cumul = x_cumul + Math.Abs(x_diff) / step_cnt; //Kun pitemmällä suunnalla one askel, lyhyempi kerryttää vähemmän, kunnes one tulee täyteen
                            }
                            else //Suunta vaihtui, joten kertymän suunta vaihtui
                            {
                                x_cumul = x_cumul - Math.Abs(x_diff) / step_cnt; //Kun pitemmällä suunnalla one askel, lyhyempi kerryttää vähemmän, kunnes one tulee täyteen
                            }
                            if (Math.Abs(x_cumul) > x_step_length)
                            {
                                z_step = 1;
                                if (x_cumul > 0) x_cumul = x_cumul - x_step_length; //Yksi askel pois kertymästä
                                if (x_cumul < 0) x_cumul = x_cumul + x_step_length; //Yksi askel pois kertymästä
                            }
                            steps.Add(new Step(x_dir, z_step, y_dir, 1)); //Y-akselille one askel, kertyvälle vasta kun one täyteen
                        }
                    }
                }
                old_coord = coord;
                old_x_dir = x_dir;
                old_y_dir = y_dir;
            }
            return steps;
        }

        async void SaveFile(string filename, string data)
        {
            try
            {
                Windows.Storage.StorageFolder localFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
                StorageFile sampleFile = await localFolder.CreateFileAsync(filename, CreationCollisionOption.ReplaceExisting);
                await FileIO.WriteTextAsync(sampleFile, data);
            }
            catch
            {
            }
        }

        async Task<string> ReadFile(string filename)
        {
            try
            {
                Windows.Storage.StorageFolder localFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
                StorageFile sampleFile = await localFolder.GetFileAsync(filename);
                String content = await FileIO.ReadTextAsync(sampleFile);
                return content;
            }
            catch (Exception)
            {
                return "error";
            }
        }
        private void AppBarButton_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(info), null);
        }
    }
}
