﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace StyroXcut
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Settings : Page
    {
        public Settings()
        {
            this.InitializeComponent();

            Loaded += Settings_Loaded;

        }

        private void Settings_Loaded(object sender, RoutedEventArgs e)
        {
            ApplicationDataContainer localSettings = ApplicationData.Current.LocalSettings;

            if (localSettings.Values["x_spr"] != null) xjako.Text = localSettings.Values["x_spr"].ToString();
            if (localSettings.Values["y_spr"] != null) yjako.Text = localSettings.Values["y_spr"].ToString();
            if (localSettings.Values["x_pitch"] != null) xkierre.Text = localSettings.Values["x_pitch"].ToString();
            if (localSettings.Values["y_pitch"] != null) ykierre.Text = localSettings.Values["y_pitch"].ToString();
            if (localSettings.Values["speed"] != null) speed.Text = localSettings.Values["speed"].ToString();
        }

        private void AsetuksetTakaisinAppBarButton_Click(object sender, RoutedEventArgs e)
        {
            Tallenna_asetukset();
            this.Frame.Navigate(typeof(MainPage), null);
        }

        private void x_thread_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            //Numero tai piste
            int i = e.Key.ToString().IndexOf("Number");
            int j = e.Key.ToString().IndexOf("190");

            if (i == -1 & j == -1)
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }

        private void x_spr_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            //Numero tai piste
            int i = e.Key.ToString().IndexOf("Number");

            if (i == -1)
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }
        }

        private void Tallenna_asetukset()
        {
            ApplicationDataContainer localSettings = ApplicationData.Current.LocalSettings;
            localSettings.Values["x_spr"] = xjako.Text;
            localSettings.Values["y_spr"] = yjako.Text;
            localSettings.Values["x_pitch"] = xkierre.Text;
            localSettings.Values["y_pitch"] = ykierre.Text;
            localSettings.Values["speed"] = speed.Text;
        }
    }

}
