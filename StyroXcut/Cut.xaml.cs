﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Windows.ApplicationModel.Resources;
using Windows.Devices.Enumeration;
using Windows.Devices.SerialCommunication;
using Windows.Foundation;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using Windows.UI.Popups;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Shapes;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace StyroXcut
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class Cut : Page
    {
        private SerialDevice serialPort = null;
        DataWriter dataWriteObject = null;
        DataReader dataReaderObject = null;
        private ObservableCollection<DeviceInformation> listOfDevices;
        private CancellationTokenSource ReadCancellationTokenSource;
        double x1 = 0, y1 = 0, x2 = 0, y2 = 0;
        double screen_unit = 1;

        string arduinoInput = "K";
        Polyline polyline1 = new Polyline();
        Polyline polyline2 = new Polyline();
        PointCollection pointCollection1 = new PointCollection();
        PointCollection pointCollection2 = new PointCollection();
        double oldx1, oldx2;
        int draw_counter;
        bool arduino = false;

        public Cut()
        {
            this.InitializeComponent();

            // Tulostusikkunan määritykset
            var uiSettings = new UISettings();
            Windows.UI.Color c = uiSettings.GetColorValue(UIColorType.Foreground);
            Brush brush = new SolidColorBrush(c);
            polyline1.Stroke = brush;
            polyline1.StrokeThickness = 2;
            polyline1.FillRule = FillRule.EvenOdd;
            polyline2.Stroke = brush;
            polyline2.StrokeThickness = 2;
            polyline2.FillRule = FillRule.EvenOdd;
            polyline1.Points = pointCollection1;
            polyline2.Points = pointCollection2;
            draw_counter = 0;
            // Tulostusikkunan määritykset loppuu

            // comPortInput.IsEnabled = false;
            listOfDevices = new ObservableCollection<DeviceInformation>();
            ListAvailablePorts();

        }

        /// <summary>
        /// ListAvailablePorts
        /// - Use SerialDevice.GetDeviceSelector to enumerate all serial devices
        /// - Attaches the DeviceInformation to the ListBox source so that DeviceIds are displayed
        /// </summary>
        private async void ListAvailablePorts()
        {
            try
            {
                string aqs = SerialDevice.GetDeviceSelector();
                var dis = await DeviceInformation.FindAllAsync(aqs);

                var resourceLoader = ResourceLoader.GetForCurrentView();
                status.Text = resourceLoader.GetString("valitseportti");

                for (int i = 0; i < dis.Count; i++)
                {
                    listOfDevices.Add(dis[i]);
                }

                DeviceListSource.Source = listOfDevices;
                //                comPortInput.IsEnabled = true;
                ConnectDevices.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                status.Text = ex.Message;
            }
        }

        /// <summary>
        /// comPortInput_Click: Action to take when 'Connect' button is clicked
        /// - Get the selected device index and use Id to create the SerialDevice object
        /// - Configure default settings for the serial port
        /// - Create the ReadCancellationTokenSource token
        /// - Start listening on the serial port input
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void comPortInput_Click(object sender, RoutedEventArgs e)
        {
            var selection = ConnectDevices.SelectedItems;

            var resourceLoader = ResourceLoader.GetForCurrentView();

            if (selection.Count <= 0)
            {

                status.Text = resourceLoader.GetString("valitseportti");
                return;
            }

            DeviceInformation entry = (DeviceInformation)selection[0];

            try
            {
                serialPort = await SerialDevice.FromIdAsync(entry.Id);
            }
            catch (Exception)
            {
                await Task.Delay(1000);
                serialPort = await SerialDevice.FromIdAsync(entry.Id);
            }

            try
            {
                if (serialPort == null) return;

                //onko arduino
                arduino = true;

                // Disable the 'Connect' button 
                comPortInput.IsEnabled = false;
                closeDevice.IsEnabled = true;

                // Configure serial settings
                serialPort.WriteTimeout = TimeSpan.FromMilliseconds(1000);
                serialPort.ReadTimeout = TimeSpan.FromMilliseconds(1000);
                serialPort.BaudRate = 115200;
                //serialPort.BaudRate = 9600;
                serialPort.Parity = SerialParity.None;
                serialPort.StopBits = SerialStopBitCount.One;
                serialPort.DataBits = 8;
                serialPort.Handshake = SerialHandshake.None;

                // Display configured settings
                status.Text = resourceLoader.GetString("porttiasetettu");
                status.Text += serialPort.BaudRate + "-";
                status.Text += serialPort.DataBits + "-";
                status.Text += serialPort.Parity.ToString() + "-";
                status.Text += serialPort.StopBits;

                // Set the RcvdText field to invoke the TextChanged callback
                // The callback launches an async Read task to wait for data
                //                rcvdText.Text = "Waiting for data...";

                // Create cancellation token object to close I/O operations when closing the device
                ReadCancellationTokenSource = new CancellationTokenSource();

                // Enable 'WRITE' button to allow sending data
                //                sendTextButton.IsEnabled = true;

                Listen();
            }
            catch (Exception ex)
            {
                status.Text = ex.Message;
                comPortInput.IsEnabled = true;
                //                sendTextButton.IsEnabled = false;
                closeDevice.IsEnabled = false;
            }
        }

        /// <summary>
        /// - Create a DataReader object
        /// - Create an async task to read from the SerialDevice InputStream
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void Listen()
        {
            try
            {
                if (serialPort != null)
                {
                    dataReaderObject = new DataReader(serialPort.InputStream);

                    // keep reading the serial input
                    while (true)
                    {
                        await ReadAsync(ReadCancellationTokenSource.Token);
                    }
                }
            }
            catch (TaskCanceledException tce)
            {
                status.Text = "Reading task was cancelled, closing device and cleaning up";
                CloseDevice();
            }
            catch (Exception ex)
            {
                //status.Foreground = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 255, 0, 0));
                status.Text = ex.Message;
            }
            finally
            {
                // Cleanup once complete
                if (dataReaderObject != null)
                {
                    dataReaderObject.DetachStream();
                    dataReaderObject = null;
                }
            }
        }

        /// <summary>
        /// ReadAsync: Task that waits on data and reads asynchronously from the serial device InputStream
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        private async Task ReadAsync(CancellationToken cancellationToken)
        {
            Task<UInt32> loadAsyncTask;

            uint ReadBufferLength = 1;

            // If task cancellation was requested, comply
            cancellationToken.ThrowIfCancellationRequested();

            // Set InputStreamOptions to complete the asynchronous read operation when one or more bytes is available
            dataReaderObject.InputStreamOptions = InputStreamOptions.Partial;

            using (var childCancellationTokenSource = CancellationTokenSource.CreateLinkedTokenSource(cancellationToken))
            {
                // Create a task object to wait for data on the serialPort.InputStream
                loadAsyncTask = dataReaderObject.LoadAsync(ReadBufferLength).AsTask(childCancellationTokenSource.Token);

                // Launch the task and wait
                UInt32 bytesRead = await loadAsyncTask;
                    if (bytesRead > 0)
                    {
                        //                    rcvdText.Text = dataReaderObject.ReadString(bytesRead);
                        //                    status.Text = dataReaderObject.ReadString(bytesRead) + " bytes read successfully!";
                        arduinoInput = dataReaderObject.ReadString(bytesRead);
                    }
            }
        }

        /// <summary>
        /// CloseDevice:
        /// - Disposes SerialDevice object
        /// - Clears the enumerated device Id list
        /// </summary>
        private void CloseDevice()
        {
            if (serialPort != null)
            {
                serialPort.Dispose();
            }
            serialPort = null;

            comPortInput.IsEnabled = true;
            //           sendTextButton.IsEnabled = false;
            //           rcvdText.Text = "";
            closeDevice.IsEnabled = false;
            listOfDevices.Clear();
        }

        /// <summary>
        /// closeDevice_Click: Action to take when 'Disconnect and Refresh List' is clicked on
        /// - Cancel all read operations
        /// - Close and dispose the SerialDevice object
        /// - Enumerate connected devices
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void closeDevice_Click(object sender, RoutedEventArgs e)
        {
            if (closeDevice.IsEnabled) // put end command
            {
                try
                {
                    byte cmd = 69; // 69 E as end
                    await Call_write(cmd, 0);
                    cmd = 67; // 67 C as close
                    await Call_write(cmd, 0);
                }
                catch
                {

                }
            }

            try
            {
                status.Text = "";
                CancelReadTask();
                await Task.Delay(100);
                CloseDevice();
                await Task.Delay(100);
                ListAvailablePorts();
            }
            catch (Exception ex)
            {
                status.Text = ex.Message;
            }
        }

        /// <summary>
        /// CancelReadTask:
        /// - Uses the ReadCancellationTokenSource to cancel read operations
        /// </summary>
        private void CancelReadTask()
        {
            if (ReadCancellationTokenSource != null)
            {
                if (!ReadCancellationTokenSource.IsCancellationRequested)
                {
                    ReadCancellationTokenSource.Cancel();
                }
            }
        }

        private void Page_SizeChanged(object sender, SizeChangedEventArgs e)
        {

        }

        private void TakaisinAppBarButton_Click(object sender, RoutedEventArgs e)
        {
            //Ensin yhteys poikki
            try
            {
                status.Text = "";
                CancelReadTask();
                CloseDevice();
                ListAvailablePorts();
            }
            catch (Exception ex)
            {
                status.Text = ex.Message;
            }

            Frame.Navigate(typeof(MainPage), null);
        }

        private void TextBlock_SelectionChanged(object sender, RoutedEventArgs e)
        {

        }

        private void ConnectDevices_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selection = ConnectDevices.SelectedItems;

            if (selection.Count > 0)
            {
                comPortInput.IsEnabled = true;
                return;
            }
            else comPortInput.IsEnabled = false;

        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {
            int step_cnt_1 = Globals.step_cnt_1.Count();
            int step_cnt_2 = Globals.step_cnt_2.Count();
            List<int> to_print_Int = new List<int>();
            List<byte> to_print_Byte = new List<byte>();

            if (step_cnt_1 > step_cnt_2)
            {
                foreach (Step step in Globals.step_cnt_1)
                {
                    int stepInt = 0;

                    //Joka askeleella liikkuu
                    if (step.X_step == 1) stepInt = stepInt + 128;
                    if (step.X_dir == 1) stepInt = stepInt + 64;
                    if (step.Y_step == 1) stepInt = stepInt + 32;
                    if (step.Y_dir == 1) stepInt = stepInt + 16;

                    to_print_Int.Add(stepInt);
                }

                double one_left_out = (double)step_cnt_2 / (double)step_cnt_1;
                double all_left_outs = 0;
                int l = 0;

                for (int i = 0; i < step_cnt_1; i++)
                {
                    int stepInt = 0;
                    if (all_left_outs < 1)
                    {
                        to_print_Int[i] = to_print_Int[i] + 0;
                    }
                    else
                    {
                        // Kun jäännöksiä yli 1, niin liikkuu
                        if (Globals.step_cnt_2[l].X_step == 1) stepInt = stepInt + 8;
                        if (Globals.step_cnt_2[l].X_dir == 1) stepInt = stepInt + 4;
                        if (Globals.step_cnt_2[l].Y_step == 1) stepInt = stepInt + 2;
                        if (Globals.step_cnt_2[l].Y_dir == 1) stepInt = stepInt + 1;
                        to_print_Int[i] = to_print_Int[i] + stepInt;
                        all_left_outs = all_left_outs - 1;
                        l++;
                    }
                    all_left_outs = all_left_outs + one_left_out;
                }
            }

            else
            {
                foreach (Step step in Globals.step_cnt_2)
                {
                    int stepInt = 0;

                    if (step.X_step == 1) stepInt = stepInt + 8;
                    if (step.X_dir == 1) stepInt = stepInt + 4;
                    if (step.Y_step == 1) stepInt = stepInt + 2;
                    if (step.Y_dir == 1) stepInt = stepInt + 1;

                    to_print_Int.Add(stepInt);
                    stepInt++;
                }

                double one_left_out = (double)step_cnt_1 / (double)step_cnt_2;
                double all_left_outs = 0;
                int l = 0;

                for (int i = 0; i < step_cnt_2; i++)
                {
                    int stepInt = 0;
                    if (all_left_outs < 1)
                    {
                        to_print_Int[i] = to_print_Int[i] + 0;
                    }
                    else
                    {
                        if (Globals.step_cnt_1[l].X_step == 1) stepInt = stepInt + 128;
                        if (Globals.step_cnt_1[l].X_dir == 1) stepInt = stepInt + 64;
                        if (Globals.step_cnt_1[l].Y_step == 1) stepInt = stepInt + 32;
                        if (Globals.step_cnt_1[l].Y_dir == 1) stepInt = stepInt + 16;
                        to_print_Int[i] = to_print_Int[i] + stepInt;
                        all_left_outs = all_left_outs - 1;
                        l++;
                    }
                    all_left_outs = all_left_outs + one_left_out;
                }
            }
            foreach (int to_print in to_print_Int)
            {
                to_print_Byte.Add((byte)to_print);
            }

            x1 = -999;
            y1 = 0;
            x2 = -999;
            y2 = 0;

            bool draw_only = false;
            if (!arduino || (simulate.IsChecked == true)) draw_only = true;

            // If connected, speed (=step delay) can be changed. Because of byte, value is devided by 100 to be < 255 (multiplied by 100 in arduino)
            if (closeDevice.IsEnabled)
            {
                double step_delay = Globals.x_pitch * 60 * 1000000 / (Globals.speed * Globals.x_spr);
                await Set_speed(step_delay);
            }

            arduinoInput = "K";
            byte cmd = 0;
            try
            {
                foreach (byte to_print in to_print_Byte)
                {
                    if (!draw_only)
                    {
                        await Call_write(77, to_print); // 77 M as move
                        Draw_image(one, to_print, x1, y1, 0, pointCollection1, polyline1);
                        Draw_image(two, to_print, x2, y2, 4, pointCollection2, polyline2);
                    }
                    else
                    {
                        Draw_image(one, to_print, x1, y1, 0, pointCollection1, polyline1);
                        Draw_image(two, to_print, x2, y2, 4, pointCollection2, polyline2);
                        await Task.Run(new Action(() =>
                        {
                            Stopwatch _sw = new System.Diagnostics.Stopwatch();
                            _sw.Start();
                            while ((_sw.Elapsed).TotalMilliseconds < 1) { }
                            _sw.Reset();
                        }));
                    }
                }
            }
            catch (Exception ex)
            {
                MessageDialog showDialog1 = new MessageDialog("Stopped?");
                var result = await showDialog1.ShowAsync();
                MessageDialog showDialog = new MessageDialog(ex.Message);
                result = await showDialog.ShowAsync();
                return;
            }

            if (closeDevice.IsEnabled) // put end command
            {
                cmd = 69;
                await Call_write(cmd, 0); // 69 E as end
            }
        }

        private async Task Call_write(byte cmd, byte to_arduino)
        {
            while (arduinoInput == "K")
            {
                await Task.Run(new Action(() =>
                {
                    Stopwatch _sw = new System.Diagnostics.Stopwatch();
                    _sw.Start();
                    while ((_sw.Elapsed).TotalMilliseconds < 1) { }
                    _sw.Reset();
                }));
            }
            // write
            await WriteAsync_Own(cmd, to_arduino);
            // wait a moment
            await Task.Run(new Action(() =>
            {
                Stopwatch _sw = new System.Diagnostics.Stopwatch();
                _sw.Start();
                while ((_sw.Elapsed).TotalMilliseconds < 2) { }
                _sw.Reset();
            }));
        }

        private async void Write_file(List<byte> to_print_Int)
        {
            FileSavePicker picker = new FileSavePicker();
            picker.FileTypeChoices.Add("file style", new string[] { ".txt" });
            picker.SuggestedStartLocation = PickerLocationId.DocumentsLibrary;
            picker.SuggestedFileName = "test.txt";
            StorageFile file = await picker.PickSaveFileAsync();
            string text_str = "";
            foreach (int number in to_print_Int)
            {
                text_str = text_str + number.ToString()/* + " " + Convert.ToString(number, 2)*/ + "\n";
            }
            await FileIO.AppendTextAsync(file, text_str);
            return;
        }

        private async Task WriteAsync_Own(byte cmd, byte byte_character)
        {
            Task<UInt32> storeAsyncTask;
            dataWriteObject = new DataWriter(serialPort.OutputStream);
            // Load the text from the sendText input text box to the dataWriter object
            //dataWriteObject.WriteByte(byte_character);
            byte[] byte_array = new byte[2];
            byte_array[0] = cmd;
            byte_array[1] = byte_character;
            dataWriteObject.WriteBytes(byte_array);

            // Launch an async task to complete the write operation
            storeAsyncTask = dataWriteObject.StoreAsync().AsTask();
            try
            {
                UInt32 bytesWritten = await storeAsyncTask;
            }
            catch (Exception ex)
            {
                string str = ex.Message;
            }
            dataWriteObject.DetachStream(); //Ilman tätä ei toimi
            dataWriteObject.Dispose();
        }

        private void Draw_image(Canvas canv, byte number, double x, double y, int bits,
            PointCollection pointCollection, Polyline polyline)
        {
            if (x == -999) //aloitus
            {
                double canvas_length = canv.ActualWidth;
                double canvas_height = canv.ActualHeight;

                x = (int)canvas_length;
                y = (int)canvas_height / 2;
                canv.Children.Clear();
                polyline.Points.Clear();
                pointCollection.Clear();
                canv.Children.Add(polyline);

                double x_step_length = Globals.x_pitch / Globals.x_spr, step_cnt = 0;
                if (Globals.length_1_C > Globals.length_2_C)
                {
                    step_cnt = Globals.length_1_C / x_step_length;
                }
                else
                {
                    step_cnt = Globals.length_2_C / x_step_length;
                }
                screen_unit = canvas_length / step_cnt;

                oldx1 = 0;
                oldx2 = 0;
            }

            string binary = Convert.ToString(number, 2);
            while (binary.Length < 8)
            {
                binary = "0" + binary;
            }

            double oldx = x, oldy = y;
            if (binary.Substring(0 + bits, 1).Equals("1") && binary.Substring(1 + bits, 1).Equals("1")) x = x + screen_unit;
            if (binary.Substring(0 + bits, 1).Equals("1") && binary.Substring(1 + bits, 1).Equals("0")) x = x - screen_unit;
            if (binary.Substring(2 + bits, 1).Equals("1") && binary.Substring(3 + bits, 1).Equals("1")) y = y - screen_unit;
            if (binary.Substring(2 + bits, 1).Equals("1") && binary.Substring(3 + bits, 1).Equals("0")) y = y + screen_unit;

            double movement_on_scr = 0, oldxx = 0;
            if (bits == 0) //akseli 1
            {
                movement_on_scr = Math.Abs(x - oldx1);
            }
            else //akseli 2
            {
                movement_on_scr = Math.Abs(x - oldx2);
            }

            if (movement_on_scr > 1)
            {
                Point point = new Point(x, y);
                pointCollection.Add(point);
                draw_counter++;
                oldxx = x;
                if (bits == 0)//akseli 1
                {
                    oldx1 = oldxx;
                }
                else//akseli 2
                {
                    oldx2 = oldxx;
                }
            }
            if (bits == 0)//akseli 1
            {
                x1 = x;
                y1 = y;
            }
            else//akseli 2
            {
                x2 = x;
                y2 = y;
            }
        }

        private async Task Set_speed(double step_delay_double)
        {
                // convert delay microseconds < 255
                int step_delay = (int)step_delay_double / 100;
                await Call_write(83, (byte)step_delay); // 83 S as speed

        }
    }
}
